FROM openjdk:8-jdk-alpine
ENV db_user="root" db_password="root" db_url="localhost" db_port="3306"
ARG JAR_FILE=artifacts/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]